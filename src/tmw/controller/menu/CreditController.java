package tmw.controller.menu;

/**
 * Interface that handle credits display.
 *
 */
public interface CreditController {

    /**
     * set the scene of credit screen.
     */
    void start();

    /**
     * return to the main menu.
     */
    void goBack();
}
