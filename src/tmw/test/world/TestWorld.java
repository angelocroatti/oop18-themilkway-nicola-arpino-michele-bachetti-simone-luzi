package tmw.test.world;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import javafx.geometry.Point2D;
import javafx.scene.shape.Rectangle;
import tmw.common.Dim2D;
import tmw.common.P2d;
import tmw.common.V2d;
import tmw.model.entities.EnemyAbbraccio;
import tmw.model.entities.GameEntity;
import tmw.model.entities.MilkEntity;
import tmw.model.entities.MilkEntityImpl;
import tmw.model.item.Item;
import tmw.model.item.powerup.SugarCanePowerUp;
import tmw.model.world.GameWorld;
import tmw.model.world.Level;

/**
 * This class is the test class for the gameWorld.
 * 
 * @version 1.2
 */

public class TestWorld {

    /**
     * Utilities to initialize test.
     *
     * @return
     */
    private GameWorld init() {
        return new Level(new Rectangle(800, 600));
    }

    /**
     * Initially world should be empty.
     */
    @Test
    public void testInitialConditions() {
        final GameWorld w = init();
        assertTrue(w.getEnemies().isEmpty());
        assertTrue(!w.getPlayer().isPresent());
        assertTrue(!w.getPlayerPosition().isPresent());
        assertTrue(w.getObstacles().isEmpty());
        assertTrue(w.getItems().isEmpty());
    }

    /**
     * Test for player entity.
     */
    @Test
    public void testPlayer() {
        final GameWorld w = init();

        w.insertPlayer((MilkEntity) new MilkEntityImpl(new P2d(0, 0), new V2d(0, 0), new Dim2D(800, 600)));

        assertTrue("player present", w.getPlayer().isPresent());
        assertTrue("pass", w.getPlayerPosition().isPresent());

        assertTrue("pass", w.getWorldArea().contains(
                new Point2D(w.getPlayer().get().getCurrentPos().getX(), w.getPlayer().get().getCurrentPos().getY())));

        w.getPlayer().get().setPos(new P2d(100000, 100000));

        assertFalse("pass", w.getWorldArea().contains(
                new Point2D(w.getPlayer().get().getCurrentPos().getX(), w.getPlayer().get().getCurrentPos().getY())));

    }

    /**
     * Test of an entity in world.
     */
    @Test
    public void testEntities() {

        final GameWorld w = init();

        final GameEntity enemy = new EnemyAbbraccio(new P2d(100, 100), new V2d(0, 0), new Dim2D(800, 600));
        w.insertEnemy(enemy);

        assertTrue("enemy present", w.getEnemies().contains(enemy));
        assertTrue("enemy in world", w.checkInBounds(enemy));

        enemy.update(Optional.ofNullable(new P2d(0, 0)));

        assertFalse(w.getEnemyPosition(enemy).get().getX() == 100);

        assertTrue("enemy in min world position", w.checkInBounds(enemy));

        enemy.destroy();

        assertTrue("enemy life at 0", enemy.getCurrentHealth() == 0);

        w.removeEnemy(enemy);

        assertTrue("enemy killed", w.getEnemies().isEmpty());

        final Item item = new SugarCanePowerUp(new P2d(10, 10), new Dim2D(800, 600));

        w.insertItem(item);

        assertTrue("item present", w.getItems().contains(item));

        w.removeItem(item);

        assertTrue("item removed", w.getItems().isEmpty());

    }

}
